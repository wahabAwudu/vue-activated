import Home from './components/home.vue'
import Info from './components/info.vue'

export default [
    {path: '/', component: Home, name: 'home'},

    // auth links
    {path: '/complete_form', component: Info, name: 'info'},
]